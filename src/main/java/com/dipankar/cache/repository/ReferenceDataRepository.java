package com.dipankar.cache.repository;

import com.dipankar.cache.entity.ReferenceData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReferenceDataRepository extends JpaRepository<ReferenceData,Integer> {
  List<ReferenceData> findByRefType(String refType);
}
