package com.dipankar.cache.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "product_cd")
    private String productCd;
    @Column(name = "product_name")
    private String productName;
    private double price;

}
