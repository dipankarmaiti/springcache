package com.dipankar.cache.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table ( name = "reference_data")
@Data
@NoArgsConstructor
public class ReferenceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column( name = "ref_type")
    private String refType;

    @Column( name = "ref_data")
    private String refData;

}
