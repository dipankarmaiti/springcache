package com.dipankar.cache.controller;

import com.dipankar.cache.entity.Product;
import com.dipankar.cache.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public String addProduct(@RequestBody Product product){
        productService.saveProduct(product);
        return "Product added successfully";
    }

    @PutMapping
    public Product updateProduct(@RequestBody Product product){
        return productService.updateProduct(product);
    }

    @GetMapping(value = "/{productCd}")
    public Product getProductDetails(@PathVariable String productCd){
        return productService.getProductDetails(productCd);
    }

    @GetMapping()
    public List<Product> getProductDetails(){
        return productService.getAllProduct();
    }

    @DeleteMapping("/{id}")
    public String deleteProduct(@PathVariable int id){
        productService.deleteProduct(id);
        return "Product got deleted";
    }

}
