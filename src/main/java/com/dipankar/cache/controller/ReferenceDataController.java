package com.dipankar.cache.controller;

import com.dipankar.cache.entity.ReferenceData;
import com.dipankar.cache.service.ReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reference")
public class ReferenceDataController {

    @Autowired
    private ReferenceDataService service;

    @PostMapping
    public String addReferenceData(@RequestBody ReferenceData referenceData){
        service.addReferenceData(referenceData);
        return "Reference Data Added Successfully";
    }

    @GetMapping("/facility")
    public List<ReferenceData> getAllFacility(){
        return service.getReferenceData("Facility");
    }

    @GetMapping("/orderStatus")
    public List<ReferenceData> getAllOrderStatus(){
        return service.getReferenceData("Order_Status");
    }

}
