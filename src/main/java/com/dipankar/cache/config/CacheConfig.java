package com.dipankar.cache.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.Date;

import static java.util.Arrays.asList;


@Configuration
@EnableCaching
@EnableScheduling
@PropertySource("application.properties")

public class CacheConfig {

    public static final String MY_KEY = "mykey";

    /*
        This Bean is commented out, because in the same application we also have redisCache implemented. In the same application we can't have
        two different cache manager.
     */
//    @Bean
//    public CacheManager cacheManager() {
//        ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager();
//        cacheManager.setCacheNames(asList("products"));
//        return cacheManager;
//    }

    @Caching(evict = {
            @CacheEvict(value="products", key = "#root.target.MY_KEY"),
            @CacheEvict(cacheNames="reference-data", allEntries=true)
    })
    @Scheduled(fixedDelay = 1 * 60 * 1000 ,  initialDelay = 500)
    public void reportCacheEvict() {
        System.out.println("Flush Cache " + new Date().toString());
    }
}