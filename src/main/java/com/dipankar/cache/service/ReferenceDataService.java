package com.dipankar.cache.service;


import com.dipankar.cache.entity.ReferenceData;
import com.dipankar.cache.repository.ReferenceDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferenceDataService {

    @Autowired
    private ReferenceDataRepository repository;

    @CachePut(value = "reference-data" , key = "#referenceData.refType" )
    public List<ReferenceData> addReferenceData(ReferenceData referenceData){
        List<ReferenceData> referenceDataList = getReferenceData(referenceData.getRefType());
        referenceDataList.add(repository.save(referenceData));
        return referenceDataList;
    }
    @Cacheable(value = "reference-data" , key = "#refType" )
    public List<ReferenceData> getReferenceData(String refType){
        System.out.println("Fetching record by making DB Call");
        return repository.findByRefType(refType);
    }

}
