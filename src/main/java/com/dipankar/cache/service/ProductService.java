package com.dipankar.cache.service;

import com.dipankar.cache.entity.Product;
import com.dipankar.cache.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ProductService {

    public static final String MY_KEY = "mykey";
    @Autowired
    private ProductRepository productRepository;

    //@CacheEvict(cacheNames="products", allEntries=true)

    /*
        If we do not rebuild the cache with same key , then if we add a new product, getAllProduct will NOT be showing the newly added product.
        So, here we have to first invoke getAllProduct() and then append the new product and then rebuild the Cache with the same key
     */
    @CachePut(value = "products" , key = "#root.target.MY_KEY")
    public List<Product> saveProduct(Product product){
        List<Product> productList = getAllProduct();
        productList.add(productRepository.save(product));
        return productList;
    }

    @CachePut(value = "products" , key = "#product.productCd" )
    public Product updateProduct(Product product){
        return productRepository.save(product);
    }

    @Cacheable(value = "products" , key = "#productCd")
    public Product getProductDetails(String productCd){
        System.out.println("Fetching product details from DB");
        return productRepository.findByProductCd(productCd);
    }
    @Cacheable(value = "products" , key = "#root.target.MY_KEY")
    public List<Product> getAllProduct(){
        System.out.println("Getting All product from DB");
        List<Product> productList = new ArrayList<>();
        productRepository.findAll().forEach(p -> {
            productList.add(p);
        });
        return productList;
    }

    //@CacheEvict(value="products", key = "#product.id" , condition="#productCd!=null")
    @CachePut(value = "products" , key = "#root.target.MY_KEY")
    public List<Product> deleteProduct(int id){
        List<Product> productList = getAllProduct();
        productRepository.deleteById(id);
//        productList.stream()
//                .filter(product -> product.getId() != id)
//                .collect(Collectors.toList());
        productList.removeIf(product -> product.getId()==id);
        return productList;
    }
}
